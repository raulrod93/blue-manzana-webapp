import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './core/auth/auth.guard.service';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () =>
      import('../app/feature/login/login.module').then((m) => m.LoginModule)
  },
  {
    path: 'productos',
    loadChildren: () =>
      import('../app/feature/products/products.module').then(
        (m) => m.ProductsModule
      ),
    canActivate: [AuthGuardService]
  },
  {
    path: 'pedidos',
    loadChildren: () =>
      import('../app/feature/orders/orders.module').then((m) => m.OrdersModule),
    canActivate: [AuthGuardService]
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
