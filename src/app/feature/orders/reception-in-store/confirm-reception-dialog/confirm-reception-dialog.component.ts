import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Order, OrderStatus, orderStatusList } from '../models/order';

interface DialogData {
  order: Order;
}

@Component({
  selector: 'app-confirm-reception-dialog',
  templateUrl: './confirm-reception-dialog.component.html',
  styleUrls: ['./confirm-reception-dialog.component.scss']
})
export class ConfirmReceptionDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ConfirmReceptionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  ngOnInit(): void {
    console.log(this.data);
  }

  get status(): string {
    return (
      orderStatusList.find((row) => row.id === this.data.order.estado)?.value ||
      ''
    );
  }
}
