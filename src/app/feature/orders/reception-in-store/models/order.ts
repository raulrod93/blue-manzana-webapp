export enum OrderStatus {
  PENDING = 1,
  CONFIRMED = 2,
  PREPARED = 3,
  SENT = 4,
  IN_DELIVERY = 5,
  DELIVERED = 6,
  RECLAIMED = 7
}

export const orderStatusList = [
  { id: OrderStatus.PENDING, value: 'Pendiente' },
  { id: OrderStatus.CONFIRMED, value: 'Confirmado' },
  { id: OrderStatus.PREPARED, value: 'Preparado' },
  { id: OrderStatus.SENT, value: 'Enviado' },
  { id: OrderStatus.IN_DELIVERY, value: 'En reparto' },
  { id: OrderStatus.DELIVERED, value: 'Entregado' },
  { id: OrderStatus.RECLAIMED, value: 'Reclamado' }
];

export interface Order {
  id: number;
  cantidad: number;
  estado: number;
  fecha: number;
  preparacion: number;
  producto: number;
  solicitadoen: number;
}
