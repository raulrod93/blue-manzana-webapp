import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { filter, first } from 'rxjs/operators';
import { UiSuccessDialogComponent } from 'src/app/core/ui/components/ui-success-dialog/ui-success-dialog.component';
import { UiWarningDialogComponent } from 'src/app/core/ui/components/ui-warning-dialog/ui-warning-dialog.component';
import { OrderService } from '../service/order.service';
import { ConfirmReceptionDialogComponent } from './confirm-reception-dialog/confirm-reception-dialog.component';
import { Order, OrderStatus } from './models/order';

@Component({
  selector: 'app-reception-in-store',
  templateUrl: './reception-in-store.component.html',
  styleUrls: ['./reception-in-store.component.scss']
})
export class ReceptionInStoreComponent {
  title = 'Recepción de pedidos';

  orderNumberControl = new FormControl();

  constructor(private dialog: MatDialog, private orderService: OrderService) {}

  searchOrder(): void {
    const orderNumber = this.orderNumberControl.value;

    this.orderService
      .getById(orderNumber)
      .pipe(first())
      .subscribe({
        next: (order: Order) => {
          if (order.estado === OrderStatus.IN_DELIVERY) {
            this.confirmOrder(order);
          } else if (order.estado !== OrderStatus.IN_DELIVERY) {
            this.orderNotInDelivery(orderNumber);
          }
        },
        error: () => {
          this.notFoundOrder(orderNumber);
        }
      });
  }

  orderNotInDelivery(orderNumber: string): void {
    this.dialog.open(UiWarningDialogComponent, {
      data: {
        title: `Pedido #${orderNumber}`,
        content: `El pedido con número #${orderNumber} no se encuentra en estado de Reparto`
      }
    });
  }

  notFoundOrder(orderNumber: string): void {
    this.dialog.open(UiWarningDialogComponent, {
      data: {
        title: `Pedido #${orderNumber}`,
        content: `No se ha encontrado el pedido con número #${orderNumber}`
      }
    });
  }

  confirmOrder(order: Order): void {
    const confirmDialog = this.dialog.open(ConfirmReceptionDialogComponent, {
      data: { order },
      width: '600px'
    });

    confirmDialog
      .afterClosed()
      .pipe(
        first(),
        filter((success) => success)
      )
      .subscribe(() => {
        this.orderService
          .confirmReception(order)
          .pipe(first())
          .subscribe(() => this.confirmReceptionSuccess(order.id));
      });
  }

  confirmReceptionSuccess(order: number): void {
    this.dialog.open(UiSuccessDialogComponent, {
      data: {
        title: `Pedido #${order}`,
        content: `Se ha confirmado la recepcion del pedido con número #${order}`
      }
    });
  }
}
