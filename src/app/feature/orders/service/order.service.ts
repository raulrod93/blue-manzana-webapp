import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Order, OrderStatus } from '../reception-in-store/models/order';

const ORDER_API = 'api/pedidos';

interface GetByIdResponse {
  data: {
    record: Order;
  };
}
@Injectable({
  providedIn: 'root'
})
export class OrderService {
  constructor(private httpClient: HttpClient) {}

  getById(id: string): Observable<Order> {
    return this.httpClient
      .get<GetByIdResponse>(`${environment.apiUrl}/${ORDER_API}/find?id=${id}`)
      .pipe(map((response: GetByIdResponse) => response.data.record));
  }

  confirmReception(order: Order): Observable<void> {
    order.estado = OrderStatus.DELIVERED;

    return this.httpClient.put<void>(
      `${environment.apiUrl}/${ORDER_API}/confirmar-entrega`,
      order
    );
  }
}
