import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReceptionInStoreComponent } from './reception-in-store/reception-in-store.component';

const routes: Routes = [
  {
    path: '',
    children: [],
    component: ReceptionInStoreComponent
  },
  {
    path: 'reception-in-store',
    children: [],
    component: ReceptionInStoreComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule {}
