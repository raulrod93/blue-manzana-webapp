import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { OrdersRoutingModule } from './orders.routing.module';
import { RouterModule } from '@angular/router';

import { OrdersComponent } from './orders.component';
import { ConfirmReceptionDialogComponent } from './reception-in-store/confirm-reception-dialog/confirm-reception-dialog.component';
import { ReceptionInStoreComponent } from './reception-in-store/reception-in-store.component';

import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [
    OrdersComponent,
    ReceptionInStoreComponent,
    ConfirmReceptionDialogComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDialogModule,
    MatDividerModule,
    OrdersRoutingModule,
    MatButtonModule,
    RouterModule
  ],
  bootstrap: [OrdersComponent]
})
export class OrdersModule {}
