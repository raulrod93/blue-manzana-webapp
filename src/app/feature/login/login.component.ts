import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
  AbstractControl
} from '@angular/forms';
import { Router } from '@angular/router';
import { finalize, first } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from 'src/app/core/auth/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';

interface User {
  username: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup = new FormGroup({});
  error = '';
  hide = true;
  submitted = false;
  loading = false;
  returnUrl = '';
  baseUrl = 'environment.baseUrl';
  hasError = false;
  loginMessageError = 'DNI o contraseña incorrectos';

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private snackbar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ])
    });
  }

  submit(): void {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    const username = this.username.value.trim();
    this.authService
      .login(username, this.password.value)
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe({
        next: (user: User) =>
          this.router.navigate([this.returnUrl || '/productos']),
        error: () => {
          this.hasError = true;
          this.snackbar.open('Login invalido', 'Cerrar', {
            duration: 3000
          });
        }
      });
  }

  get username(): AbstractControl {
    return this.form.get('username') as AbstractControl;
  }
  get usernameErrorMessage(): string {
    if (
      this.username.invalid &&
      (this.username.dirty || this.username.touched)
    ) {
      if (this.username.hasError('required')) {
        return 'Campo obligatorio.';
      }
      if (this.username.hasError('minlength')) {
        return 'El usuario es muy corto.';
      }
    }
    return '';
  }

  get password(): AbstractControl {
    return this.form.get('password') as AbstractControl;
  }

  get passwordErrorMessage(): string {
    if (
      this.password.invalid &&
      (this.password.dirty || this.password.touched)
    ) {
      if (this.password.hasError('required')) {
        return 'Campo obligatorio.';
      }
      if (this.password.hasError('minlength')) {
        return 'La contraseña es muy corta.';
      }
    }
    return '';
  }
}
