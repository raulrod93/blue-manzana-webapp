import { Product } from './product';
import { Store } from './store';

export interface StoreStock {
  cantidad: number;

  producto: Product;

  tienda: Store;
}
