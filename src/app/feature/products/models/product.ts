export interface Product {
  id: string;
  categoria: string;
  categoriaDescripcion: string;
  nombre: string;
  precio: number;
}
