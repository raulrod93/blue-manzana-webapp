interface Address {
  calle: string;
  numero: number;
  codigopostal: number;
  otros: string;
  localidad: string;
  provincia: string;
}

export interface Store {
  id: string;
  telefono: string;
  imagen?: string;
  direccion: Address;
}
