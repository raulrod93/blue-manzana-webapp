import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ProductsRoutingModule } from './products.routing.module';
import { CommonModule } from '@angular/common';

import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';

import { UiSuccessDialogComponent } from 'src/app/core/ui/components/ui-success-dialog/ui-success-dialog.component';
import { UiWarningDialogComponent } from 'src/app/core/ui/components/ui-warning-dialog/ui-warning-dialog.component';
import { CatalogueProductComponent } from './catalogue/catalogue-product/catalogue-product.component';
import { CatalogueComponent } from './catalogue/catalogue.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { StoreModalComponent } from './product-detail/store-modal/store-modal.component';
import { ProductsComponent } from './products.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [
    ProductsComponent,
    CatalogueComponent,
    CatalogueProductComponent,
    ProductDetailComponent,
    StoreModalComponent,
    UiWarningDialogComponent,
    UiSuccessDialogComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatDividerModule,
    MatButtonModule,
    RouterModule,
    RouterModule,
    ProductsRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule
  ],
  bootstrap: [ProductsComponent]
})
export class ProductsModule {}
