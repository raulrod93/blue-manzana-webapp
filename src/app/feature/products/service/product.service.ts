import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Product } from '../models/product';

const PRODUCT_API = 'api/productos';
const MOCK_PRODUCTS = [
  {
    id: '1',
    category: 'Clothes',
    description: 'Pantalones rojos',
    price: 23.0
  },
  {
    id: '1',
    category: 'Clothes',
    description: 'Pantalones rojos',
    price: 23.0
  },
  {
    id: '1',
    category: 'Clothes',
    description: 'Pantalones rojos',
    price: 23.0
  },
  {
    id: '1',
    category: 'Clothes',
    description: 'Pantalones rojos',
    price: 23.0
  },
  {
    id: '1',
    category: 'Clothes',
    description: 'Pantalones rojos',
    price: 23.0
  },
  { id: '1', category: 'Clothes', description: 'Pantalones rojos', price: 23.0 }
];

const MOCK_PRODUCT = {
  id: '1',
  category: 'Clothes',
  description: 'Pantalones rojos',
  price: 23.0
};

interface ProductFilterResponse {
  data: {
    filter: unknown;
    records: Product[];
  };
}
interface ProductResponse {
  data: Product;
}

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(private http: HttpClient) {}

  filter(): Observable<Product[]> {
    return this.http
      .get<ProductFilterResponse>(`${environment.apiUrl}/${PRODUCT_API}/filter`)
      .pipe(map((response: ProductFilterResponse) => response.data.records));
  }
}
