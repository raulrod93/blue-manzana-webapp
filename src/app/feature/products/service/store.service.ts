import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { StoreStock } from '../models/storeStock';

const STORE_API = 'api/tiendas';

interface GetStoreStockResponse {
  data: {
    filter: {
      productoId: string;
    };
    records: StoreStock[];
  };
}

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  constructor(private http: HttpClient) {}

  getStoreStocks(productoId: string): Observable<StoreStock[]> {
    return this.http
      .get<GetStoreStockResponse>(
        `${environment.apiUrl}/${STORE_API}/stock?productoId=${productoId}`
      )
      .pipe(map((response: GetStoreStockResponse) => response.data.records));
  }
}
