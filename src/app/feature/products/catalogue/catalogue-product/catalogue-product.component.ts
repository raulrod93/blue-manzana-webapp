import { Component, Input } from '@angular/core';
import { Product } from '../../models/product';

@Component({
  selector: 'app-catalogue-product',
  templateUrl: './catalogue-product.component.html',
  styleUrls: ['./catalogue-product.component.scss']
})
export class CatalogueProductComponent {
  @Input() producto: Product = {
    id: '',
    categoriaDescripcion: '',
    categoria: '',
    nombre: '',
    precio: 0
  };
}
