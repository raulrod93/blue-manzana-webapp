import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { Product } from '../models/product';
import { ProductService } from '../service/product.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss']
})
export class CatalogueComponent implements OnInit {
  title = 'Catalogo de productos';

  productos: Product[] = [];

  constructor(public productService: ProductService) {}

  ngOnInit(): void {
    this.productService
      .filter()
      .pipe(first())
      .subscribe((productos: Product[]) => (this.productos = productos));
  }
}
