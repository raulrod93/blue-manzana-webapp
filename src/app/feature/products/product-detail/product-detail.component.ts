import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { finalize, first } from 'rxjs/operators';
import { Product } from '../models/product';
import { StoreStock } from '../models/storeStock';
import { StoreService } from '../service/store.service';
import { StoreModalComponent } from './store-modal/store-modal.component';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  producto!: Product;

  loadingProduct = true;
  loadingStore = false;

  existenciasEnTienda!: StoreStock[];

  constructor(
    private dialog: MatDialog,
    private storeService: StoreService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.producto = this.router.getCurrentNavigation()?.extras.state?.producto;
    this.loadingProduct = false;
  }

  ngOnInit(): void {
    if (!this.producto) {
      this.router.navigate(['/productos']);
    }

    this.fetchProduct();
  }

  fetchProduct(): void {
    const productId = this.activatedRoute.snapshot.params.id;

    this.loadingStore = true;
    this.storeService
      .getStoreStocks(productId)
      .pipe(
        first(),
        finalize(() => (this.loadingStore = false))
      )
      .subscribe({
        next: (existenciasEnTienda: StoreStock[]) =>
          (this.existenciasEnTienda = existenciasEnTienda),
        error: (error) => console.error(error)
      });
  }

  openStoreDetail(existenciasEnTienda: StoreStock): void {
    this.dialog.open(StoreModalComponent, {
      data: { existenciasEnTienda },
      width: '800px'
    });
  }

  getDireccion(existenciasEnTienda: StoreStock): string {
    const { calle, numero, codigopostal, otros, localidad, provincia } =
      existenciasEnTienda.tienda.direccion;
    return [calle, numero, codigopostal, otros, localidad, provincia]
      .filter((value) => value !== null)
      .join(', ');
  }
}
