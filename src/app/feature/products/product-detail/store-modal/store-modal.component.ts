import { Component, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StoreStock } from '../../models/storeStock';

interface DialogData {
  existenciasEnTienda: StoreStock;
}

@Component({
  selector: 'app-store-modal',
  templateUrl: './store-modal.component.html',
  styleUrls: ['./store-modal.component.scss']
})
export class StoreModalComponent {
  constructor(
    public dialogRef: MatDialogRef<StoreModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  get status(): string {
    if (!this.data.existenciasEnTienda) {
      return 'No disponible';
    }
    return this.data.existenciasEnTienda.cantidad > 3
      ? 'Disponible'
      : 'Pocas existencias ⚠️';
  }

  get direccion(): string {
    const { calle, numero, codigopostal, otros, localidad, provincia } =
      this.data.existenciasEnTienda.tienda.direccion;
    return [calle, numero, codigopostal, otros, localidad, provincia]
      .filter((value) => value !== null)
      .join(', ');
    // return `${calle}, ${numero}, ${codigopostal}, ${otros}, ${localidad}, ${provincia}`;
  }
}
