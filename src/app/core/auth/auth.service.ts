import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';

const API_URL_USER_LOGIN = '/login';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) {}

  login(username: string, password: string): any {
    return this.http
      .post<any>(
        `${environment.apiUrl}${API_URL_USER_LOGIN}`,
        { username, password },
        { observe: 'response' }
      )
      .pipe(
        tap((res) => {
          const { headers } = res;
          const token = headers.get('Authorization') || '';
          localStorage.setItem('token', token);
        })
      );
  }
}
