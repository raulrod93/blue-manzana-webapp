import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private router: Router) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    next.handle(request).pipe(
      catchError((err) => {
        if ([401, 403].indexOf(err.status) !== -1) {
          // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
          this.router.navigate(['/login']);
        }

        return throwError(err);
      })
    );
    if (!request.url.endsWith('/login')) {
      const Authorization = localStorage.getItem('token');

      if (!Authorization) {
        this.router.navigate(['/login']);
      }

      const isApiUrl = request.url.startsWith(environment.apiUrl);

      if (Authorization && isApiUrl) {
        request = request.clone({ setHeaders: { Authorization } });
      }
    }

    return next.handle(request);
  }
}
