import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

interface DialogData {
  title: string;
  content: string;
}

@Component({
  selector: 'app-ui-warning-dialog',
  templateUrl: './ui-warning-dialog.component.html',
  styleUrls: ['./ui-warning-dialog.component.scss']
})
export class UiWarningDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<UiWarningDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  ngOnInit(): void {}
}
