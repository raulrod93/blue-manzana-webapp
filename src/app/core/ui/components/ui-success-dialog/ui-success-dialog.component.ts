import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

interface DialogData {
  title: string;
  content: string;
}

@Component({
  selector: 'app-ui-success-dialog',
  templateUrl: './ui-success-dialog.component.html',
  styleUrls: ['./ui-success-dialog.component.scss']
})
export class UiSuccessDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<UiSuccessDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  ngOnInit(): void {}
}
